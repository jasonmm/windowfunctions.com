* Over

*** Question 0
#+BEGIN_SRC sql
select name, sum(weight) over (order by name)
from cats
order by name
#+END_SRC

*** Question 1
#+BEGIN_SRC sql
select name,
        breed,
        sum(weight) over (partition by breed order by name) as running_total_weight
from cats
order by breed, name
#+END_SRC

*** Question 2
#+BEGIN_SRC sql
select name,
        weight,
        avg(weight) over (order by weight ROWS between 1 preceding and 1 following)
from cats
order by weight
#+END_SRC

*** Question 3

https://www.windowfunctions.com/questions/over/3

#+BEGIN_SRC sql
select name,
        sum(weight) over (order by weight desc ROWS unbounded preceding)
from cats
order by weight desc
#+END_SRC
